const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class RewardNominators extends Model {
    static associate(models) {
      RewardNominators.belongsTo(models.users, {
        foreignKey: "user_id",
      });
      RewardNominators.belongsTo(models.rewards, {
        foreignKey: "reward_id",
      });
    }
  }
  RewardNominators.init(
    {
      id: {
        field: "id",
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
      },
      userId: {
        field: "user_id",
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "users",
          key: "id",
        },
      },
      rewardId: {
        field: "reward_id",
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "rewards",
          key: "id",
        },
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      }
    },
    {
      sequelize,
      modelName: "reward_nominators"
    }
  );
  return RewardNominators;
};

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const errorCode = require('./common/errorCode');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.api = {
    success: true,
    error: {
      code: "",
      message: "",
      details: [
        {
          target: "",
          message: "",
        },
      ],
    },
    data: {},
    statusCode: 200,
  };
  next();
});

require("./handlers/V1")(app);

// error handler
app.use(function (err, req, res, next) {
  if (!err) {
    return next();
  }

  if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
    res.api.success = false;
    res.api.error.code = errorCode.BAD_REQUEST;
    res.api.error.message = "Invalid input";
    res.api.error.details[0].target = "body";
    res.api.error.details[0].message = err.type;
    res.api.data = {};
    res.api.statusCode = 400;
    res.status(res.api.statusCode);

    return res.send(res.api);
  }
  res.api.success = false;
  res.api.error.code = errorCode.INTERNAL_SERVER_ERROR;
  res.api.error.message = "Oops! something broke";
  res.api.error.details = [];
  res.api.data = {};
  res.api.statusCode = 500;
  res.status(res.api.statusCode);

  return res.send(res.api);
});

app.listen(3001, () => {
  console.log("App started");
});

module.exports = app;

import React from "react";
import { Switch } from "react-router-dom";
import Home from "pages/Home/Home";
import RewardsPage from "pages/Rewards/Rewards";
import Dashboard from "pages/Dashboard/Dashboard";
import CreateReward from "pages/CreateReward/CreateReward";
import Route from "./Route";

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home as any} hideHeader />
      <Route path="/Rewards" exact component={RewardsPage as any} hideHeader />
      <Route path="/dashboard" exact component={Dashboard as any} hideHeader />
      <Route
        path="/createreward"
        exact
        component={CreateReward as any}
        hideHeader
      />
    </Switch>
  );
}

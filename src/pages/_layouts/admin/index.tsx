import React, { ReactNode } from "react";
import PropTypes from "prop-types";
import AdminHeader from "components/AdminHeader";
import AdminFooter from "components/AdminFooter";
import { Wrapper } from "styled";
import Loader from "components/Loader";
import ScrollToTop from "components/scrollToTop";
import Toast from "components/Toast";

interface Props {
  children: ReactNode;
}

export default function AdminLayout({ children }: Props) {
  return (
    <Wrapper className="d-flex flex-column">
      <AdminHeader />
      <ScrollToTop />
      <Toast />
      <main className="d-flex flex-column flex-grow-1" id="maincontent">
        {children}
      </main>
      <Loader />
      <AdminFooter />
    </Wrapper>
  );
}
AdminLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

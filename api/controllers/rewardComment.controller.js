const express = require("express");

const router = express.Router();
const rewardComment = require("../repo/rewardComment.repo");
const errorCode = require("../common/error");
const errorhandler = require("../common/errorhandler");
const successhandler = require("../common/successhandler");


router.post("/", async (req, res, next) => {
  try {
    let result;

    result = await rewardComment.postRewardComments(req.body);

    const resobj = {
      body: {
        result,
      },
    };

    return successhandler.sendSuccessResponse(resobj, res);

  } catch (error) {
    next(error);
  }
  return true;
});

module.exports = router;
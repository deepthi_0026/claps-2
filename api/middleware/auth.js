const jwt = require("jsonwebtoken");
const errorhandler = require("../common/errorhandler");
const errorCode = require("../common/error");

exports.authMiddleware = function (req, res, next) {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    jwt.verify(authHeader, "JWT_SECRET", (err, requestMeta) => {
      if (err) {
        const errobj = {
          statusCode: 401,
          code: errorCode.UNAUTHORIZED,
          details: [
            {
              target: "token",
              message: "unauthorized",
            },
          ],
        };
        return errorhandler.sendErrorResponse(errobj, res);
      }
      req.headers.requestMeta = requestMeta;
      next();
      return true;
    });
  }
};

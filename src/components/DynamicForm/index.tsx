import React, { useEffect, useState } from "react";

const DynamicForm = (props: any) => {
  const [formData, setFormData] = useState<any>(false);
  const [checkboxGroupRequired, setCheckboxGroupRequired] =
    useState<any>(false);

  const { model } = props;
  const { className } = props;
  const { length } = formData;
  const { disabled } = props;
  let onChange: Function;
  useEffect(() => {
    setFormData(model);
  }, [model, formData]);
  const renderForm = () => {
    const formModel = formData || props.model;
    const formUI = formModel.map((object: any) => {
      const { key } = object;
      const type = object.type || "text";
      const formProps = object.props || {};
      const { name } = object;
      const { value } = object;
      const target = key;
      let input = (
        <input
          {...formProps}
          className="form-control"
          type={type}
          key={key}
          id={key}
          name={name}
          defaultValue={value}
          onChange={(e) => {
            onChange(e, target);
          }}
        />
      );

      if (type === "parent") {
        input = object.options.map((option: any) => {
          return (
            <React.Fragment key={`fr${option.key}`}>
              <div className="col-sm-6 col-md-12 col-xl-6 d-sm-flex mb-3">
                <label
                  className="col-md-4 col-xl-6 px-0"
                  key={`ll${option.key}`}
                  htmlFor={`ll${option.key}`}
                >
                  {option.label}
                </label>
                <div className="col-xl-6 col-md-8 px-0 pl-sm-3">
                  <input
                    {...formProps}
                    className="form-control"
                    type={option.type}
                    key={option.key}
                    name={option.name}
                    defaultValue={option.value}
                    id={`ll${option.key}`}
                    onChange={(e) => {
                      onChange(e, object.key, "single", option.key);
                    }}
                  />
                </div>
              </div>
            </React.Fragment>
          );
        });
        input = <div className="row pr-0">{input}</div>;
      }

      if (type === "radio") {
        input = object.options.map((option: any, index: any) => {
          const checked = option.value === object.value;
          return (
            <React.Fragment key={`fr${option.key}`}>
              <div className="col-md-6 mb-4">
                <div className="form-radio">
                  <input
                    {...formProps}
                    id={option.key}
                    className="form-input"
                    type={type}
                    key={option.key}
                    name={object.key}
                    defaultValue={option.value}
                    defaultChecked={option.value === object.value}
                    onChange={(e) => {
                      onChange(e, object.key);
                    }}
                  />
                  <label htmlFor={option.key} key={`ll${option.key}`}>
                    {option.label}
                  </label>
                </div>
              </div>
            </React.Fragment>
          );
        });
        input = (
          <div className="row" role="radiogroup">
            {input}
          </div>
        );
      }

      if (type === "select") {
        input = object.options.map((option: any) => {
          const checked = option.value === value;
          return (
            <option
              {...formProps}
              className="form-input"
              key={option.key}
              value={option.value}
            >
              {option.value}
            </option>
          );
        });

        input = (
          <select
            value={value}
            onChange={(e) => {
              onChange(e, object.key);
            }}
          >
            {input}
          </select>
        );
      }

      if (type === "checkbox") {
        input = object.options.map((option: any) => {
          let checked = false;
          const childProps = option.props || {};
          if (value && value.length > 0) {
            checked = value.indexOf(option.value) > -1;
          }
          return (
            <React.Fragment key={`cfr${option.key}`}>
              <div className="col-lg-6">
                <div className="form-group form-check">
                  <input
                    {...childProps}
                    id={option.key}
                    type={type}
                    key={option.key}
                    name={option.name}
                    required={checkboxGroupRequired}
                    defaultChecked={checked}
                    defaultValue={option.value}
                    onChange={(e) => {
                      onChange(e, object.key, "multiple");
                    }}
                  />
                  <label key={`ll${option.key}`} htmlFor={option.key}>
                    {option.label}
                  </label>
                </div>
              </div>
            </React.Fragment>
          );
        });

        input = (
          <div className="row" role="group">
            {input}
          </div>
        );
      }

      return (
        <div>
          {type === "parent" || type === "radio" || type === "checkbox" ? (
            <div
              key={key}
              className={
                object.props && object.props.required
                  ? "form-group row required"
                  : "form-group row"
              }
              role="group"
              aria-labelledby={key}
            >
              <label htmlFor={object.label} className="col-md-5" id={key}>
                {object.label}
              </label>
              <div className="col-md-7 col-lg-6">{input}</div>
            </div>
          ) : (
            <div
              key={key}
              className={
                object.props && object.props.required
                  ? "form-group row required"
                  : "form-group row"
              }
            >
              <label htmlFor={key} className="col-md-5">
                {object.label}
              </label>
              <div className="col-md-7 col-lg-6">{input}</div>
            </div>
          )}
        </div>
      );
    });
    return formUI;
  };
  onChange = (
    e: any,
    key: string,
    type: string = "single",
    childKey: string = ""
  ) => {
    let dataObj = formData.filter((data: any) => {
      return data.key === key;
    });

    if (childKey && dataObj[0] && dataObj[0].options) {
      dataObj = dataObj[0].options.filter((data: any) => {
        return data.key === childKey;
      });
    }

    if (dataObj && dataObj.length > 0) {
      if (type === "single") {
        dataObj[0].value = e.target.value;
      } else {
        if (e.target.checked === true) {
          dataObj[0].value.push(e.target.value);
        } else {
          const currIndex = dataObj[0].value.indexOf(e.target.value);
          dataObj[0].value.splice(currIndex, 1);
        }
        if (
          dataObj[0].props &&
          dataObj[0].props.required === true &&
          dataObj[0].value.length === 0
        ) {
          setCheckboxGroupRequired(true);
        } else {
          setCheckboxGroupRequired(false);
        }
      }
    }
    setFormData(formData);
    if (type === "multiple") {
      renderForm();
    }
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    if (props.onSubmit) {
      props.onSubmit(formData);
    }
  };

  const goBack = () => {
    props.goBack();
  };

  return (
    <div className={className}>
      {length ? (
        <form
          autoComplete="off"
          className="dynamic-form"
          onSubmit={(e) => {
            onSubmit(e);
          }}
        >
          {renderForm()}
          <div className="reg-btn row mx-0">
            <div className="col-md-6 offset-md-5 px-0 px-md-3">
              <button
                type="button"
                onClick={goBack}
                className="btn btn-round btn-secondary mr-0 mr-md-3 mb-4 mb-md-0"
                aria-label="back to Program selection page"
              >
                Back
              </button>
              <button
                disabled={disabled}
                type="submit"
                className="btn btn-round btn-primary ml-0 ml-md-3"
                aria-label="Continue to Agreement page"
              >
                Continue
              </button>
            </div>
          </div>
        </form>
      ) : null}
    </div>
  );
};
DynamicForm.defaultProps = {
  model: [],
};
export default DynamicForm;

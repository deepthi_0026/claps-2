const express = require("express");

const router = express.Router();
const user = require("../repo/user.repo");
const errorCode = require("../common/error");
const errorhandler = require("../common/errorhandler");
const successhandler = require("../common/successhandler");


router.get("/:userId", async (req, res, next) => {
  try {
    let result;

    result = await user.findUser(req.params.userId);

    if (result == null){
      result = await user.findAllUsers();
    }

    const resobj = {
      body: {
        result,
      },
    };

    return successhandler.sendSuccessResponse(resobj, res);

  } catch (error) {
    next(error);
  }
  return true;
});

router.post("/", async (req, res, next) => {
  try {

    const result = await user.postUser(req.body);

    const resobj = {
      body: {
        user: result,
      },
    };

    return successhandler.sendSuccessResponse(resobj, res);

  } catch (error) {
    next(error);
  }
  return true;
});

module.exports = router;

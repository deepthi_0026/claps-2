const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {}
  }
  Users.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        field: "name",
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      email: {
        field: "email",
        type: DataTypes.JSON(),
        allowNull: false,
      },
      sso_id: {
        field: "sso_id",
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      }
    },
    {
      sequelize,
      modelName: "users"
    }
  );
  return Users;
};

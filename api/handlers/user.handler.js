const express = require("express");

const router = express.Router();

const userController = require("../controllers/index").user;

router.use("/users", userController);

module.exports = router;

/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const CreateRewardWrapper = styled.div`
  .bgcolor {
    background: linear-gradient(
      140deg,
      #be10e1 0%,
      #bc70a4 50%,
      #d641bd 75%
    ) !important;
    padding: 10px;
  }
  .bgcolor h1 {
    color: #fff;
    font-size: 35px;
  }
  .claps-logo {
    background: url(../images/logo.png) no-repeat;
    background-size: contain;
    width: 20rem;
    min-height: 5.5625rem;
    position: relative;
    color: #222328;
  }
  .claps-logo:before {
    position: absolute;
    width: 0.0625rem;
    height: 5.5625rem;
    background-color: #e3e3e3;
    left: 6.375rem;
    top: 0;
    content: "";
  }
  .claps-logo:after {
    content: "Claps";
    font-size: 1.375rem;
    font-weight: 600;
    position: absolute;
    left: 6.875rem;
    top: 1.8125rem;
  }
  .aui-sidenav {
    background-color: #fff;
  }
  .MuiButton-label {
    text-transform: capitalize;
  }
  .buttonstyle {
    text-align: right;
  }
  .buttonstyle button {
    margin: 10px;
    width: 190px;
    color: #fff;
    font-size: 16px;
    background-color: #7103d3;
  }
  a {
    color: #fff;
    text-decoration: none;
  }
  .mauto {
    margin: 0 auto;
    background-color: #7103d3;
    color: #fff;
  }
  .aui-sidenav {
    width: 17%;
  }
  .aui-main-header.aui-pri-header {
    border-bottom: 2px solid #c91ebc;
  }
  select.form-control:read-only {
    background-color: #fff;
    cursor: default;
    border: 1px solid #ced4da;
  }
  .awardimg {
    border-radius: 50%;
    padding: 10px;
    box-shadow: 0px 3px 6px #00000029;
    margin: 5px;
  }
  @media (min-width: 768px) {
    .aui-modal .modal-body + .modal-footer {
      margin-top: 0;
    }
  }
  .dropdown-toggle::after {
    display: inline-block;
    margin-left: 0.255em;
    vertical-align: 0.255em;
    content: "";
    border-top: 0.3em solid;
    border-bottom: 0;
    border-left: 0.3em solid transparent;
    border-right: 0;
  }
  .box {
    border: 1px solid #bcc3ca;
    border-radius: 6px;
    padding: 0 0 15px;
  }
  .projbtn {
    background-color: #f8914c !important;
    color: #fff;
    width: 100px;
  }
`;

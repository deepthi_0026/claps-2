const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const type = DataTypes.ENUM("SPOT", "NOMINATION");
  const nomination = DataTypes.ENUM("INDIVIDUAL", "TEAM");

  class Rewards extends Model {
    static associate(models) {}
  }
  Rewards.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        field: "name",
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      type: {
        field: "type",
        type: type,
        defaultValue: "SPOT",
        allowNull: false,
      },
      nomination: {
        field: "nomination",
        type: nomination,
        defaultValue: "INDIVIDUAL",
        allowNull: false,
      },
      claps: {
        field: "claps",
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      createdBy: {
        field: "created_by",
        type: DataTypes.STRING,
        allowNull: false,
      },
      updatedBy: {
        field: "updated_by",
        type: DataTypes.STRING,
        allowNull: true,
      },
      rewardedTo: {
        field: "rewarded_to",
        type: DataTypes.JSON(),
        allowNull: true,
      },
      rewardNominatorsId: {
        field: "reward_nominators_id",
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        allowNull: true,
      },
      nominiesCount: {
        field: "nominies_count",
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      autoPublish: {
        field: "auto_publish",
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      published: {
        field: "published",
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      startDate: {
        field: "start_date",
        type: DataTypes.DATE,
        allowNull: true,
      },
      endDate: {
        field: "end_date",
        type: DataTypes.DATE,
        allowNull: true,
      },
      isActive: {
        field: "is_active",
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      isDelete: {
        field: "is_delete",
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "rewards"
    }
  );
  return Rewards;
};
